var arr = [];
function B1(){
    var total =0
    arr.forEach(function(number){
        if(number>0)
            total+=number;
    })
    document.querySelector(".displayB1").innerHTML =`<h5 class="text-primary mt-3">Tổng số dương ${total}</h5>`;
}

function B2(){
    var count =0;
    for (let i =0;i<arr.length;i++){
        if(arr[i]>0)
            count++;
    }
    document.querySelector(".displayB2").innerHTML =`<h5 class="text-primary mt-3">Có ${count} số dương </h5>`;
}
// B3
function B3(){
    let min = Math.min.apply(null,arr);
    document.querySelector(".displayB3").innerHTML =`<h5 class="text-primary mt-3">Số bé nhất trong mảng: ${min}</h5>`;
}
//b4
function B4(){
    min = arr[0];
    for (let i =1;i<arr.length;i++){
        if(arr[i]<min&&arr[i]>0)
            min=arr[i];
    }
    document.querySelector(".displayB4").innerHTML =`<h5 class="text-primary mt-3">Số  dương bé nhất trong mảng: ${min}</h5>`;
}

// b5
function B5(){
    var result = -1;
    for (let i =arr.length-1;i>=0;i--){
        if(arr[i]%2==0){
            result = arr[i];
            break;
        }
    }
    document.querySelector(".displayB5").innerHTML =`<h5 class="text-primary mt-3">Số chẵn cuối cùng: ${result}</h5>`;
}
//b6
function B6(){
    var num1 = document.querySelector("#position1").value-1;
    var num2 = document.querySelector("#position2").value-1;
    var temp = arr[num1];
    arr[num1] = arr[num2];
    arr[num2] = temp;
    document.querySelector(".displayB6").innerHTML =`<h5 class="text-primary mt-3">Mảng sau khi đổi chỗ: ${arr}</h5>`;
}
//b7
function B7(){
    arr.sort(function(a,b){return a-b});
    document.querySelector(".displayB7").innerHTML =`<h5 class="text-primary mt-3">Mảng sau khi sắp xếp: ${arr}</h5>`;
}
//b8
function check(n)
{
    var flag = true;
    if (n < 2){
        flag = false;
    }
    else if (n == 2){
        flag = true;
    }
    else if (n % 2 == 0){
      flag = false;
    }
    else{
        for (var i = 3; i < n-1; i+=2)
        {
            if (n % i == 0){
                flag = false;
                break;
            }
        }
    }
    return flag;
}
function B8(){
    var result =-1;
    for (let i =0;i<arr.length;i++){
        if(check(arr[i]))
            result = arr[i];
    }
    document.querySelector(".displayB8").innerHTML =`<h5 class="text-primary mt-3">Số nguyên tố đầu tiên: ${result}</h5>`;
}
//bai9
var arr2=[];
function arrB9(){
    arr2.push( document.querySelector("#themSo").value*1);
    document.querySelector("#themSo").value="";
    document.querySelector(".displayArr").innerHTML =`<h5 class="text-primary mt-3">Mảng hiện tại: ${arr2}</h5>`;
}
function B9(){
    var count=0;
    arr2.forEach(function(number){
        if(Number.isInteger(number))
            count++;
    });
    document.querySelector(".displayB9").innerHTML =`<h5 class="text-primary mt-3">Mảng có : ${count} số nguyên</h5>`;
}
// b10
function B10(){
    var soDuong=0;
    var soAm=0;
    var result ="bằng";
    for (let i =0;i<arr.length;i++){
        if (arr[i]>0)
            soDuong++;
        if (arr[i]<0)
            soAm++;      
    }
    if (soDuong>soAm)
        result="lớn hơn";
    if (soDuong<soAm)
        result="bé hơn";
    document.querySelector(".displayB10").innerHTML =`<h5 class="text-primary mt-3">Số dương trong mảng ${result} số âm trong mảng </h5>`;
}
function themSo(){
    var number = document.querySelector("#number").value*1;
    document.querySelector("#number").value="";
    arr.push(number);
    document.querySelector(".display").innerHTML =`<h5 class="text-primary mt-3">Mảng hiện tại: ${arr}</h5>`;
}